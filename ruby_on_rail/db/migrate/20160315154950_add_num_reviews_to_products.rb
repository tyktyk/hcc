class AddNumReviewsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :num_reviews, :float
  end
end
